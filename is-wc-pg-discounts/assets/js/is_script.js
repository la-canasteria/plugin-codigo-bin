(function($) {



    'use strict';



    $(function() {

        let l_dsto_normal = 0;

        $(document.body).on('focusout', '#mp-card-number', function() {
            let l_is_wc_h_number = jQuery("#is-wc-h-number").val();
            let l_mp_card_number = jQuery("#mp-card-number").val();
            //console.log('l_mp_card_number=' + l_mp_card_number);
            l_mp_card_number = l_mp_card_number.replace(/ /g, ''); //Quitando espacio en blanco            
            //console.log('========Inicio ....=========');
            //console.log('l_is_wc_h_number=' + l_is_wc_h_number);
            //console.log('l_mp_card_number sin espacio=' + l_mp_card_number);
            //console.log("siguiente");
            let tipo_descuento = -1;
            let array_descuento = [];
            if (f_valida_tarjeta(l_mp_card_number)) {

                if (sessionStorage.getItem("inicio") == 1) {
                    //console.log("termina proceso");
                    sessionStorage.removeItem("inicio");
                } else {
                    if (f_valida_tarjeta(l_mp_card_number)) {
                        jQuery.ajax({
                            type: "post",
                            async: false,
                            url: ajax_var.url,
                            data: "action=" + ajax_var.action + '&operacion=banco' + "&nonce=" + ajax_var.nonce + "&bin_code=" + l_mp_card_number,
                            success: function(result) {
                                console.log("result==" + result);
                                let aux_valor = result.split(";");
                                let variable_banco = aux_valor[1];
                                //console.log("blanco", variable_banco);
                                //console.log("aux ", aux_valor[5]);
                                if (variable_banco != '') {
                                    jQuery(".mensaje").html = "";
                                    jQuery(".modal_titulo").html = "";
                                    jQuery(".modal_titulo").html('¡ERROR!')
                                    jQuery(".mensaje").html('Número de tarjeta incorrecto.</br><b>Este pedido solo se puede pagar con ' + variable_banco + '.</b><br/>Por favor, inténtelo nuevamente.');
                                    jQuery(".fondo_transparente")[0].style.display = 'block';
                                    jQuery("#mp-card-number").val("");
                                    jQuery("#is-wc-h-number").val("");
                                } else {
                                    jQuery("#mp-card-holder-name").focus();
                                    jQuery("#is-wc-d-pedidos").val(aux_valor[0]);
                                    let json = JSON.parse(aux_valor[0]);
                                    tipo_descuento = json["Detalles"][0]["TipoDescuento"];
                                    let tipo_descuento_json = json["Detalles"];
                                    for (let index = 0; index < tipo_descuento_json.length; index++) {
                                        array_descuento.push(tipo_descuento_json[index]["TipoDescuento"]);
                                    }
                                    //console.log("array descuento", array_descuento);
                                    let array_descuento_dos = array_descuento.sort();
                                    //console.log("array descuento sort", array_descuento_dos);
                                    tipo_descuento = array_descuento_dos[array_descuento_dos.length - 1];
                                    //console.log("dato mayor", array_descuento_dos[array_descuento_dos.length - 1]);
                                    //console.log("detalles json", tipo_descuento_json);
                                    //console.log(json["Detalles"][0]["TipoDescuento"])
                                    //console.log(json["Detalles"][0].TipoDescuento)
                                    //console.log("otro campo", json["Detalles"][0]["DescripcionArticulo"]);

                                }

                            },
                            complete: function() {
                                jQuery("#is-wc-h-number").val(l_mp_card_number);
                                let tmp_card_number;
                                jQuery(document.body).one('update_checkout', function() {
                                    tmp_card_number = jQuery("#mp-card-number").val();
                                    //console.log('update_checkout 01 :' + tmp_card_number);
                                });

                                jQuery(document.body).one('updated_checkout', function() {
                                    jQuery("#mp-card-number").val(tmp_card_number);
                                    //console.log('update_checkout 02 :' + tmp_card_number);
                                });

                                let encuentra_valor = -1;
                                let encuentra_valor2 = -1;
                                for (let index = 0; index < array_descuento.length; index++) {
                                    if (array_descuento[index] == 1) {
                                        encuentra_valor = 1;
                                        l_dsto_normal = 1;
                                    }
                                    if (array_descuento[index] == 2) {
                                        encuentra_valor2 = 1;
                                    }
                                }
                                //console.log("encuentra valor 1", encuentra_valor);
                                //console.log("encuentra valor 2", encuentra_valor2);
                                if (encuentra_valor == 1) {
                                    jQuery(document.body).one('updated_checkout', function() {
                                        //console.log('-- update_checkout 03 -- ');
                                        //console.log('[*focusout 01] - inicia');
                                        $("#mp-card-number").trigger("focusout");
                                        //console.log('[*focusout 01] - Fin');
                                    });
                                    jQuery('body').trigger('update_checkout');
                                    sessionStorage.setItem("inicio", 1);
                                } else {
                                    if (encuentra_valor2 == 1) {

                                    } else {
                                        //console.log("entrando al else");
                                        if (l_dsto_normal == 1) {
                                            jQuery(document.body).trigger('update_checkout');
                                        }
                                        l_dsto_normal = 0;
                                        jQuery(document.body).on('updated_checkout', function() {
                                            // console.log("se ejecuto updated_checkout");

                                        });
                                    }
                                }
                            },
                            error: function(xhr) {
                                //console.log("error", xhr.statusText + xhr.responseText);

                            }
                        });
                    } else {
                        //console.log("tarjeta incorrecta");
                    }

                }
            } else {
                //console.log("tarjeta incorrecta 2")
            }

        });











        $(document.body).on('change', '#mp-select-tipo_promocion_pago', function() {



            let optVal = this.value;



            jQuery.ajax({



                type: "post",



                url: ajax_var.url,



                data: "action=" + ajax_var.action + "&operacion=cb" + "&valorSelect=" + optVal + "&nonce=" + ajax_var.nonce,



                success: function(result) {



                    if (jQuery("#is-wc-h-number").length > 0) {



                        jQuery("#is-wc-h-number").val('');



                    }



                    ;







                    /*



                     if (optVal == 1) {



                     jQuery("#is-wc-h-number").val('');



                     } else {



                     jQuery("#is-wc-h-number").val('');



                     }



                     */



                    jQuery("#div-mp-select_000").html(result);



                    $('body').trigger('update_checkout');



                }



            });







        });







        $(document.body).on('change', '#payment_method_woo-mercado-pago-ticket', function() {



            jQuery("#is-wc-h-number").val('');



            jQuery("#mp-card-number").val('');



            $('body').trigger('update_checkout');



        });







        /*$(document.body).on('change', 'input[name=payment-method]', function () {



            jQuery("#is-wc-h-number").val('');



            jQuery("#mp-card-number").val('');



            alert('hola');



            $('body').trigger('update_checkout');



        });*/







        //valida tarjeta



        function f_valida_tarjeta(l_cadena_validar) {



            if (l_cadena_validar.length >= 6) {



                return true;



            } else {



                return false;



            }



        }







        function f_es_igual_tarjeta(l_anterior, l_nueva) {



            if (l_anterior == l_nueva) {



                return true;



            } else {



                return false;



            }







        }







    });











}(jQuery));







function MiFuncionJS(l_cadena_mensaje) {



    alert(l_cadena_mensaje);



}



;



function f_ajax_cupones(l_operacion) { //MiFuncionJS2



    jQuery.ajax({



        type: "post",



        url: ajax_var.url,



        data: "action=" + ajax_var.action + "&operacion=" + l_operacion + "&nonce=" + ajax_var.nonce,



        success: function(result) {



            jQuery("#div-mp-select_000").html(result);



            if (jQuery("#is-wc-h-number").length > 0) {



                jQuery("#is-wc-h-number").val('');



                //$('body').trigger('update_checkout');



            }



            ;



        }



    });



}