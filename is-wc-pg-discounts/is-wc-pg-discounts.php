<?php





/*


 * Plugin Name: Descuentos Por codigo BIM


 * Plugin URI: **************************


 * Description: Descuentos por codigo BIM


 * Version: 1.2.0


 * Author: IDE SOLUTION


 * Author URI: **************************


 */





/*


  Copyright (C) 2020 IDE SOLUTION


 */





// If this file is called directly, abort.


if (!defined('ABSPATH')) {


    exit;


}





if (!defined('WPINC')) {


    die;


}





if (!defined('IS_WC_PG_D_PLUGIN_URL')) {


    define('IS_WC_PG_D_PLUGIN_URL', plugin_dir_url(__FILE__));


}





/**


 * Notice about unsupported PHP version.  ??????


 */


function is_wc_pg_discounts_unsupported_php_version_notice() {


    $message = esc_html__('Modulo de Descuentos para WooCommerce requiere PHP version 5.6 o superior. Por favor actualice su version de PHP ', 'is-wc-pg-discounts'); ///


    echo $message;


}





// Check for PHP version and throw notice. ?????????


if (version_compare(PHP_VERSION, '5.6', '<=')) {


    add_action('admin_notices', 'is_wc_pg_discounts_unsupported_php_version_notice');


    return;


}





/**


 * Curl validation   ???????


 */


function is_wc_pg_discounts_notify_curl_error() {


    $message = __('Modulo de Descuentos  Error: PHP Extension CURL no esta instalado.', 'is-wc-pg-discounts');


    echo $message;


}





if (!in_array('curl', get_loaded_extensions())) {


    add_action('admin_notices', 'is_wc_pg_discounts_notify_curl_error');


    return;


}





/**  ????????


 * Summary: Places a warning error to notify user that WooCommerce is missing.


 * Description: Places a warning error to notify user that WooCommerce is missing.


 */


function is_wc_pg_discounts_notify_woocommerce_miss() {


    $message = sprintf(


            __('The payment module of Woo Mercado depends on the latest version of %s to run!', 'is-wc-pg-discounts'),


            ' <a href="https://wordpress.org/extend/plugins/woocommerce/">WooCommerce</a>'


    );


    echo $message;


}





$all_plugins = apply_filters('active_plugins', get_option('active_plugins'));


if (!stripos(implode($all_plugins), 'woocommerce.php')) {


    add_action('admin_notices', 'is_wc_pg_discounts_notify_woocommerce_miss');


    return;


}





// Load module class if it wasn't loaded yet.


if (!class_exists('Cl_Is_Wc_Pg_Discounts')) {


    /**


     * Cl-Is-Wc-Pg-Discounts Class.


     */


    require_once dirname(__FILE__) . '/includes/Cl-Is-Wc-Pg-Discounts.php';


    /**


     * Initialize the plugin actions.


     */


    add_action('plugins_loaded', array('Cl_Is_Wc_Pg_Discounts', 'get_instance'));


    //register_activation_hook( __FILE__, 'activate_wad' );


}


/*


register_activation_hook( __FILE__, 'activate_wad' );


register_deactivation_hook( __FILE__, 'deactivate_wad' );


  */


/*


function activate_wad() {


  require_once plugin_dir_path( __FILE__ ) . 'includes/class-wad-activator.php';


  Wad_Activator::activate();


}


 * 


 */





/**


 * The code that runs during plugin deactivation.


 * This action is documented in includes/class-wad-deactivator.php


 */


/*


function deactivate_wad() {


  require_once plugin_dir_path( __FILE__ ) . 'includes/class-wad-deactivator.php';


  Wad_Deactivator::deactivate();


}


 


  private function init_hooks() {


    register_activation_hook( WC_PLUGIN_FILE, array( 'WC_Install', 'install' ) );


    


  }


  


*/


