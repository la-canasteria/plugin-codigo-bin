<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Cl_Is_Wc_Pg_Add_Discounts {

    public function __construct() {
           //Load public-facing JavaScript.
        add_action('wp_enqueue_scripts', array($this, 'f_enqueue_scripts')); //Agrega script : is_script.js
        ///Agregado campo hidden                
        add_action('woocommerce_after_order_notes', array(&$this, 'f_custom_before_checkout_billing_form'));
        //Agregando Funcionalidad de cargo/descuento
        add_action('woocommerce_cart_calculate_fees', array(&$this, 'f_woocommerce_custom_surcharge'));
        //Metodo para pasar el valor de descuento
        add_action('woocommerce_checkout_update_order_meta', array(&$this, 'f_claserama_save_additional_fields_checkout'));

        //SOLO PARA PRUEBA
        //add_action( 'woocommerce_before_cart', array(&$this, 'F_bbloomer_print_cart_array') );
        //add_filter('woocommerce_billing_fields', array(&$this, 'custom_woocommerce_billing_fields'));
        //add_filter('woocommerce_checkout_fields', array(&$this, 'custom_woocommerce_billing_fields2'));
        //add_action('woocommerce_after_checkout_form', array(&$this, 'ts_after_checkout_form'), 10);
        //add_action('woocommerce_checkout_after_order_review', array(&$this, 'ts_after_checkout_form'));
        //add_action('woocommerce_checkout_before_customer_details', array(&$this, 'ts_after_checkout_form2'));
        //add_action('woocommerce_after_order_notes', array(&$this, 'ts_after_checkout_form'));
        //
        add_action('woocommerce_checkout_order_review', array(&$this, 'f_ts_after_checkout_form'), 10);
        add_action('woocommerce_checkout_update_order_review', array($this, 'f_checkout_update_order_review'));
        add_action('woocommerce_checkout_process', array(&$this, 'misha_check_if_selected'));
        //add_action('woocommerce_checkout_update_order_review', array(&$this, 'f_ts_after_checkout_form'), 10);
        ////        
        //add_action('woocommerce_checkout_order_review', array(&$this, 'f_ts_after_checkout_form'), 10);
        //add_action('woocommerce_review_order_after_cart_contents', array(&$this, 'f_ts_after_checkout_form'), 10);
        // add_action('woocommerce_applied_coupon', array(&$this, 'apply_product_on_coupon'));
        // add_action('woocommerce_removed_coupon', array(&$this, 'apply_product_remover_coupon'));
    }

    /*

     */

    function apply_product_remover_coupon() {
        //echo '<h2>AAAAA remover.</h2>';
        //echo '<script language="javascript">alert("Quitando cupon y metodo para validar....");</script>';
        // $this->ts_after_checkout_form();
    }

    function apply_product_on_coupon() {
        //echo '<h2>AAAAA on_coupon.</h2>';
        // echo '<script language="javascript">alert("Agregando cupon y metodo para validar....");</script>';
        /* echo '<div id="dialogConfirm"></div>';
          echo '<script language="javascript">';
          //echo 'alert("Agregando cupon y metodo para validar....");';
          echo '$("#dialogConfirm").dialog({
          resizable: false,
          height: 140,
          modal: true,
          title: "Mensaje Modal",
          buttons: {
          "Aceptar": function () {
          $(this).dialog("close");
          },
          "Cancelar": function () {
          $(this).dialog("close");
          }
          }
          });';
          echo 'S$(function () {
          ShowDialog();
          });';
          echo '</script>';
         */
        //echo '<script language="javascript">';
        //echo 'alert("Agregando cupon y metodo para validar....");';
        //echo '</script">';
        //$this->ts_after_checkout_form();
    }

    function misha_check_if_selected() {

        // you can add any custom validations here
        if (!empty($_POST['mp-select-tipo_promocion_pago'])) {
            wc_add_notice('valor enviado:' . ($_POST['mp-select-tipo_promocion_pago']), 'error');
        }
        wc_add_notice('No dejar pasar. Enviado......', 'error');
    }

    /*
      function ts_after_checkout_form2() {
      echo '<h2>AAAAA AAA.</h2>';
      }

     */

    function ts_after_checkout_form() {
        try {

            //echo '<h2>Hay producti qye tiene promocion y cupon , pero solo puede elegir uno solo.</h2>';
            /// VALIDA SI ALGUN PRODUCTO TIENE UNA LINEA CON DESCUENTO POR TARJETA
            $l_cantidad_cupones_tiene_carrito = $this->f_cantidad_cupones_tiene_carrito();
            $l_cantidad_productos_con_promocion_descuentos = $this->f_cantidad_productos_con_promocion_descuento_tarjeta();
            $l_cantidad_productos_con_promocion_cupones = $this->f_cantidad_productos_con_promocion_cupones();


            $l_cantidad_productos_con_promocion_descuentos = 1; //temporal
            $l_cantidad_cupones_tiene_carrito=0;

            if ($l_cantidad_cupones_tiene_carrito > 0) {//Tiene cupon agregado el carrito
                if ($l_cantidad_productos_con_promocion_descuentos > 0) {//Existe producto con promociones de descuento                    
                    $l_mensaje = 'Producto con descuento por pagar con tarjeta1: \n';
                    $l_mensaje .= '*  VISA - CREDITO - AADVANTAGE® GOLD - BBVA - LÍNEA GOURMET (8%) \n';
                    $l_mensaje .= '*  VISA - CREDITO - ORO - SCOTIBANK - DESTILADOS (15%)  \n\n';
                    $l_mensaje .= '* Conocerás los montos de descuento al ingresar su número de tarjeta.  ';

                    //Agregar texto y combo para elegir
                    $l_label_campo = 'Productos tiene cupon/descuento, debe de Elegir solo una opcion';
                    woocommerce_form_field('mp-select-tipo_promocion_pago', array(
                        'type' => 'select',
                        'class' => array('form-row-wide my-select'),
                        // 'label'             => 'Dropdown',
                        'label' => "$l_label_campo",
                        'options' => array('1' => 'Aplicar Descuento con Cupon', '2' => 'Aplicar Descuento por pago de Tarjeta',),
                        'required' => true
                            )
                            , 1
                            // , $checkout->get_value( 'my_field_name' )
                    );
                    echo '<script language="javascript">alert("' . $l_mensaje . '");</script>';
                } else {//No existe producto con promociones de descuento
                    // No agrega nada - ok                    
                }
            } else {//No tienen cupones agregado al carrito
                /// aun no se hace nada
                if ($l_cantidad_productos_con_promocion_descuentos > 0) {//Existe producto con promociones de descuento
                    $l_mensaje = 'Tu compra tiene descuentos al pagar con esta(s) tarjeta(s): \n';
                   $l_mensaje .= '*  VISA - CREDITO - AADVANTAGE® GOLD - BBVA - LÍNEA GOURMET (8%) \n';
                    $l_mensaje .= '*  VISA - CREDITO - ORO - SCOTIBANK - DESTILADOS (15%)  \n\n';
                    $l_mensaje .= '* Visualizaras los descuentos al ingresar el número de tu tarjeta.';

                    //Agregar texto y combo para elegir
                    $l_label_campo = 'Productos tiene cupon/descuento, debe de Elegir solo una opcion';
                    woocommerce_form_field('mp-select-tipo_promocion_pago', array(
                        'type' => 'select',
                        'class' => array('form-row-wide my-select'),
                        // 'label'             => 'Dropdown',
                        'label' => "$l_label_campo",
                        'options' => array('1' => 'Aplicar Descuento con Cupon', '2' => 'Aplicar Descuento por pago de Tarjeta'), //'1' => 'Aplicar Descuento con Cupon',
                        'required' => true
                            )
                            , 2
                            // , $checkout->get_value( 'my_field_name' )
                    );
                    //echo $l_label_campo';

                    echo '<script language="javascript">alert("' . $l_mensaje . '");</script>';
                } else {//No existe producto con promociones de descuento
                    // No agrega nada - ok                    
                }
            }
        } catch (Exception $ex) {
            do_action('logger', 'ts_after_checkout_form...');
            do_action('logger', $exc->getMessage(), 'error');
        }
    }

    function f_cantidad_productos_con_promocion_descuento_tarjeta() {
        $l_cantidad_producto_con_descuento = 1;
        try {
            global $woocommerce;
            foreach ($woocommerce->cart->get_cart() as $cart_item_key => $cart_item) {
                $_product = $cart_item ['data'];
                //Formando la cadena de categorias separados por coma
                $l_string_categorias = '';
                $l_separador = ',';
                foreach ($_product->get_category_ids() as $category_id) {
                    if ($l_string_categorias == '') {
                        $l_string_categorias = $category_id;
                    } else {
                        $l_string_categorias = $l_string_categorias . $l_separador . $category_id;
                    }
                }
            }
            do_action('logger', '$l_string_categorias==' . $l_string_categorias, 'warning');
            if ($l_string_categorias != '') {
                //llamar al Webservice para indicar si hay descuento, que devuelve la cantidad de lineas con descuentos , envia repetido las lineas en caso de haber
                //$l_resultado = $this->f_ws_customer_gets_discount($l_bin_hash, $category_id);
                //$l_hay_algun_descuento = 0; //No hay descuento , seteo para probar
                $l_cantidad_producto_con_descuento = 1; //hay descuento , seteo para probar
            }
            return $l_cantidad_producto_con_descuento;
        } catch (Exception $ex) {
            do_action('logger', 'f_cantidad_productos_con_descuento...');
            throw $exc;
        }
    }

    function f_cantidad_productos_con_promocion_cupones() {
        //Obtener si carrito tiene descuenot

        $l_cantidad_producto_con_cupones = 0;
        try {
            //Obtener los cupones por articulos -- For
            ////$l_cantidad_producto_con_cupones = 1; //para prueba 

            return $l_cantidad_producto_con_cupones;
        } catch (Exception $ex) {
            do_action('logger', 'f_cantidad_productos_con_descuento...');
            throw $exc;
        }
    }

    function f_cantidad_cupones_tiene_carrito() {
        //Obtener si carrito tiene descuenot

        $l_cantidad_cupon_agregados = 0;
        try {
            $l_cupones_agregados = WC()->cart->get_applied_coupons();
            if (!empty($l_cupones_agregados)) {
                $l_cantidad_cupon_agregados = count($l_cupones_agregados);
            }

            //$l_cantidad_cupon_agregados = 1; //para prueba 

            return $l_cantidad_cupon_agregados;
        } catch (Exception $ex) {
            do_action('logger', 'f_cantidad_productos_con_descuento...');
            throw $exc;
        }
    }

    /*
      function custom_woocommerce_billing_fields2($fields) {
      $fields['billing']['billing_options'] = array(
      'label' => __('NIF2', 'woocommerce'), // Add custom field label
      'placeholder' => _x('Your NIF here....', 'placeholder', 'woocommerce'), // Add custom field placeholder
      'required' => false, // if field is required or not
      'clear' => false, // add clear or not
      'type' => 'text', // add field type
      'class' => array('my-css')   // add class name
      );

      return $fields;
      }
     */

    /*
      function custom_woocommerce_billing_fields($fields) {

      $fields['billing_options'] = array(
      'label' => __('NIF3', 'woocommerce'), // Add custom field label
      'placeholder' => _x('Your NIF2 here....', 'placeholder', 'woocommerce'), // Add custom field placeholder
      'required' => false, // if field is required or not
      'clear' => false, // add clear or not
      'type' => 'text', // add field type
      'class' => array('my-css')    // add class name
      );

      return $fields;
      }

     */

    /**
     * Script file included
     */
    public function f_enqueue_scripts() {
        if (is_checkout()) {//  Devuelve verdadero en la pagina de pago.          
            //agregar script js : is_script.js
            wp_enqueue_script('is-wc-pg-discounts-js', plugins_url('assets/js/is_script.js', plugin_dir_path(__FILE__)), array('wc-checkout'), false);
        }
    }

    /**
     * Agregando campos al Formulario de pago
     */
    function f_custom_before_checkout_billing_form($checkout) {
        //campo de codigo bin
        echo '<input type="hidden" class="input-hidden" name="is_wc_h_number" id="is-wc-h-number" value="" />';
        //campo de codig de descuento
        echo '<input type="text" class="input-hidden" name="is_wc_h_discount_code" id="is_wc_h_discount_code" value="" />';
    }

    /**
     * Agregue un recargo/descuento a su carrito/pago
     * invocando al WS
     */
    function f_woocommerce_custom_surcharge($cart) {
        //do_action('logger', 'Inicio 01....woocommerce_custom_surcharge');
        if (is_admin() && !defined('DOING_AJAX')) {//|| is_cart()            
            return;
        }
        //define('WP_DEBUG', true); // activa el Modo Debug en tu sitio web
        //do_action('logger', 'Inicio 03....woocommerce_custom_surcharge');

        if (isset($_POST['post_data'])) {
            parse_str($_POST['post_data'], $post_data);
        } else {
            $post_data = $_POST; // fallback for final checkout (non-ajax)
        }


        //do_action('logger', 'lwoocommerce_custom_surcharge:[is_wc_h_number]=' . $post_data['is_wc_h_number']);
        do_action('logger', 'lwoocommerce_custom_surcharge:[mp-select-tipo_promocion_pago]=' . $post_data['mp-select-tipo_promocion_pago']);

        if (isset($post_data['is_wc_h_number'])) {// verificae filter, por le momento envia numero de tarjeta
            $aux = $post_data['is_wc_h_number'];
            $aux = str_replace(' ', '', $aux); //Quitar espacio en blanco
            //do_action('logger', '$aux=' . $aux);

            if (strlen($aux) >= 6) {//o validar el codigo BIN??
                $l_bin = substr($aux, 0, 6); //4963211212, obtener BIM
                //do_action('logger', '$l_bin=' . $l_bin);
                $l_bin_hash = hash('sha224', $l_bin);

                try {
                    //recorrer el carrito los productos 
                    //$l_categoria_producto
                    do_action('logger', 'Inicio recorrido...Inicio');
                    global $woocommerce;
                    foreach ($woocommerce->cart->get_cart() as $cart_item_key => $cart_item) {
                        $_product = $cart_item ['data'];
                        /*
                          do_action('logger', '====================================================', 'warning');
                          do_action('logger', 'Inicio recorrido...$_product2==' . ($cart_item['product_id']), 'warning'); //OK
                          do_action('logger', 'Inicio recorrido...quantity2==' . ($cart_item['quantity']), 'warning'); //OK
                          do_action('logger', 'Inicio recorrido...name2==' . ($cart_item['name']), 'warning'); //NO DATA
                          do_action('logger', 'Inicio recorrido...subtotal2==' . ($cart_item['subtotal']), 'warning'); //NO DATA
                          do_action('logger', 'Inicio recorrido...total_tax2==' . ($cart_item['total_tax']), 'warning'); //NO DATA
                          do_action('logger', 'Inicio recorrido...sku2==' . ($cart_item['sku']), 'warning'); //NO DATA
                          //do_action('logger', 'Inicio recorrido...data2==' . ($cart_item['data']), 'warning');
                          do_action('logger', 'Inicio recorrido...key2==' . ($cart_item['key']), 'warning'); //OK
                          do_action('logger', 'Inicio recorrido...price2==' . ($cart_item['price']), 'warning'); //NO DATA
                          do_action('logger', 'Inicio recorrido...regular_price2==' . ($cart_item['regular_price']), 'warning'); //NO DATA
                          do_action('logger', 'Inicio recorrido...data_hash2==' . ($cart_item['data_hash']), 'warning'); //OK

                          do_action('logger', 'Inicio recorrido...$_product->id=' . ($_product->id), 'warning'); //IDENTIFICADOR DE itEM
                          do_action('logger', 'Inicio recorrido...$_product->product_id=' . ($_product->product_id), 'warning'); //NO DATA
                          do_action('logger', 'Inicio recorrido...$_product->name=' . ($_product->name), 'warning'); //OK
                          do_action('logger', 'Inicio recorrido...$_product->subtotal=' . ($_product->subtotal), 'warning'); //NO DATA
                          do_action('logger', 'Inicio recorrido...$_product->total_tax=' . ($_product->total_tax), 'warning'); //NO DATA
                          do_action('logger', 'Inicio recorrido...$_product->quantity=' . ($_product->quantity), 'warning'); //NO DATA
                          do_action('logger', 'Inicio recorrido...$_product->tax_class=' . ($_product->tax_class), 'warning'); //NO DATA
                          do_action('logger', 'Inicio recorrido...$_product->sku=' . ($_product->sku), 'warning'); //OK
                          do_action('logger', 'Inicio recorrido...$_product->key=' . ($_product->key), 'warning'); //NO DATA
                          do_action('logger', 'Inicio recorrido...$_product->quantity=' . ($_product->quantity), 'warning'); //NO DATA
                          do_action('logger', 'Inicio recorrido...$_product->data=' . ($_product->data), 'warning'); //NO DATA
                          do_action('logger', 'Inicio recorrido...$_product->data_hash=' . ($_product->data_hash), 'warning'); //NO DATA
                          do_action('logger', 'Inicio recorrido...$_product->get_price=' . ($_product->get_price()), 'warning'); //OK
                          do_action('logger', 'Inicio recorrido...$_product->get_title=' . ($_product->get_title()), 'warning'); //OK
                          do_action('logger', 'Inicio recorrido...$_product->get_regular_price=' . ($_product->get_regular_price()), 'warning'); //OK
                          do_action('logger', 'Inicio recorrido...$_product->get_sale_price=' . ($_product->get_sale_price()), 'warning'); ////NO DATA
                          do_action('logger', 'Inicio recorrido...$_product->get_regular_price=' . ($_product->get_regular_price()), 'warning'); //OK
                          do_action('logger', 'Inicio recorrido...$_product->get_total_sales=' . ($_product->get_total_sales()), 'warning'); //OK
                          do_action('logger', 'Inicio recorrido...$_product->get_tax_status=' . ($_product->get_tax_status()), 'warning'); //OK
                          do_action('logger', 'Inicio recorrido...$_product->get_tax_class=' . ($_product->get_tax_class()), 'warning'); //NO DATA
                          do_action('logger', 'Inicio recorrido...$_product->get_stock_quantity=' . ($_product->get_stock_quantity()), 'warning'); //NO DATA
                          do_action('logger', 'Inicio recorrido...$_product->get_stock_status=' . ($_product->get_stock_status()), 'warning'); //OK
                          do_action('logger', 'Inicio recorrido...$_product->get_parent_id=' . ($_product->get_parent_id()), 'warning'); //OK

                         */
                        $l_quantity = $cart_item['quantity'];
                        $l_price = $cart_item['data']->get_price();


                        foreach ($_product->get_category_ids() as $category_id) {
                            do_action('logger', 'Inicio recorrido...$category_id=' . $category_id, 'warning');
                            $l_auxiliar_categoria = get_term_by('id', $category_id, 'product_cat');

                            /*
                              do_action('logger', 'Inicio recorrido...$l_auxiliar_categroria-->name=' . $l_auxiliar_categoria->name, 'warning');
                              do_action('logger', 'Inicio recorrido...$l_auxiliar_categroria-->count=' . $l_auxiliar_categoria->count, 'warning');
                              do_action('logger', 'Inicio recorrido...$l_auxiliar_categroria-->parent=' . $l_auxiliar_categoria->parent, 'warning');
                             */


                            $l_resultado = $this->f_ws_customer_gets_discount($l_bin_hash, $category_id);

                            if (isset($l_resultado)) {
                                $l_tipo_error = $l_resultado->ConsultaDescuentoResult->Errws->TipoErr;

                                if ($l_tipo_error == 0) {
                                    $l_codigo_descuento = $l_resultado->ConsultaDescuentoResult->Descuento->Codigo;
                                    if ($l_codigo_descuento > 0) {
                                        //
                                        /////update_post_meta($order_id, 'codigo descuento', sanitize_text_field('1000000'));
                                        do_action('logger', '$l_codigo_descuento > 0 - ok');

                                        $l_discount_name = $l_resultado->ConsultaDescuentoResult->Descuento->Descripcion;
                                        $l_percentage_discount = $l_resultado->ConsultaDescuentoResult->Descuento->Porcentaje;
                                        if ($l_percentage_discount > 0) {//Validando descuento positivo
                                            $l_discount_name_to_show = $l_discount_name . ' (-' . ($l_percentage_discount ) . '%)' . ($_product->id) . '/' . ($_product->name) . '/' . $category_id;
                                            //$l_discount_name_to_show = $l_discount_name . ' (-' . ($l_percentage_discount ) . '%)';
                                            $cart_discount = $this->f_calculate_discount_amount($l_percentage_discount, ($l_quantity * $l_price)) * - 1;
                                            $cart->add_fee($l_discount_name_to_show, $cart_discount, true);
                                            do_action('logger', '$l_percentage_discount > 0 - ok ok');
                                            /* ANTERIOR
                                              $l_discount_name_to_show = $l_discount_name . ' (-' . ($l_percentage_discount ) . '%)';
                                              $cart_discount = $this->f_calculate_discount_amount($l_percentage_discount, $cart->cart_contents_total) * - 1;
                                              $cart->add_fee($l_discount_name_to_show, $cart_discount, true);
                                              do_action('logger', '$l_percentage_discount > 0 - ok ok');
                                             */
                                        }
                                    } else {
                                        do_action('logger', 'sin descuento:' . $l_descripcion . '. de :' . $l_porcentaje . '%.', 'warning');
                                    }
                                } else {//Hubo Error
                                    $l_codigo_error = $l_resultado->ConsultaDescuentoResult->Errws->CodigoErr;
                                    $l_descripcion_error = $l_resultado->ConsultaDescuentoResult->Errws->DescripcionErr;
                                    do_action('logger', 'Error :' . $l_descripcion_error, 'error');
                                }
                            } else {
                                do_action('logger', 'else 01 :isset($l_resultado)', 'error');
                            }
                        }
                        do_action('logger', '====================================================', 'warning');
                    }
                    do_action('logger', 'Inicio recorrido...Fin');
                } catch (Exception $exc) {
                    do_action('logger', 'catch 100 : Exception', 'error');
                    do_action('logger', $exc->getMessage(), 'error');
                }
            } else {
                do_action('logger', 'else 02 :strlen($aux) >= 6');
            }
        } else {
            do_action('logger', 'else 03 :isset($post_data[is_wc_h_number])');
        }
    }

    /**
     * Guardar los campos adicionales cuando el usuario termine el proceso de checkout
     */
    function f_claserama_save_additional_fields_checkout($order_id) {
        do_action('logger', 'f_claserama_save_additional_fields_checkout--Inicia');
        if (!empty($_POST['is_wc_h_discount_code'])) {
            try {
                do_action('logger', 'f_claserama_save_additional_fields_checkout--is_wc_h_discount_code:' . ($_POST['is_wc_h_discount_code']));
                update_post_meta($order_id, 'codigo descuento', sanitize_text_field($_POST['is_wc_h_discount_code']));
            } catch (Exception $exc) {
                do_action('logger', $exc->getMessage());
            }
        }
        do_action('logger', 'f_claserama_save_additional_fields_checkout--Fin');
    }

    /*
     * $l_bin_hash: codigo bin encriptado
     * $l_categoria: codigo de categoria del producto
     */

    public function f_ws_customer_gets_discount($l_bin_hash, $l_categoria_producto) {
        try {
            //[01] Seteando valores
            date_default_timezone_set('America/Lima');
            $l_hoy_fecha = date("d/m/Y");
            $l_hoy_hora = date("H:i:s");

            $l_servidor = 'http://app01.ide-solution.com';
            $l_puerto = '80';
            $l_contexto = 'ws_consulta_descuento';
            $l_servicio = 'WebService';

            //[02] Obteniendo nombres de wsdl y end point
            $l_nombre_wsdl = $l_servicio . '.asmx?wsdl';
            $l_nombre_asmx = $l_servicio . '.asmx';

            //[03] Generando nombre de WSL y location
            $l_wsdl = $l_servidor . ':' . $l_puerto . '/' . $l_contexto . '/' . $l_nombre_wsdl;
            $l_location = $l_servidor . ':' . $l_puerto . '/' . $l_contexto . '/' . $l_nombre_asmx;

            //do_action('logger', 'f_ws_customer_gets_discount-$l_wsdl==' . $l_wsdl);
            //[04] Parametros de conexion de WS
            ini_set("default_socket_timeout", 15);
            $l_params = array(
                'trace' => true,
                'location' => $l_location,
                'soap_version' => SOAP_1_2,
                'cache_wsdl' => WSDL_CACHE_NONE,
                'exceptions' => true
                    // ,'connection_timeout' => 15
                    )
            ;

            //[05] Conectando con WS
            $client = new SoapClient($l_wsdl, $l_params);

            //[06] Preparando valores de envio       
            $l_datos_envio = array('nuntarjeta' => $l_bin_hash, 'categoria' => $l_categoria_producto, 'fecha' => $l_hoy_fecha, 'hora' => $l_hoy_hora);
            //
            //[07] consumiendo Metodo ConsultaDescuento del WS
            $result = $client->ConsultaDescuento($l_datos_envio);
            return $result;
        } catch (Exception $exc) {
            do_action('logger', 'Error f_ws_customer_gets_discount...');
            throw $exc;
        }
    }

    /**
     * Calcule the discount amount.
     */
    protected function f_calculate_discount_amount($value, $subtotal) {
        $value = ( $subtotal / 100 ) * ( $value );
        return $value;
    }

    /** SOLO PAR PRUEBA
     * @snippet       See what is inside the Cart array - WooCommerce
     * @how-to        Get CustomizeWoo.com FREE
     * @sourcecode    https://businessbloomer.com/?p=21941
     * @author        Rodolfo Melogli
     * @compatible    WC 2.6.14, WP 4.7.2, PHP 5.5.9
     */
    function F_bbloomer_print_cart_array() {
        $cart = WC()->cart->get_cart();
        print_r($cart);
    }

}

new Cl_Is_Wc_Pg_Add_Discounts();
/*
  do_action('logger', 'Inicio ....');

  $l_wsdl = "http://app01.ide-solution.com/ws_consulta_descuento/WebService.asmx?wsdl";
  $l_location = 'http://app01.ide-solution.com/ws_consulta_descuento/WebService.asmx';

  $l_opts_ssl = array(
  'ssl' => array(
  'ciphers' => 'RC4-SHA',
  'verify_peer' => false,
  'verify_peer_name' => false,
  'allow_self_signed' => true,
  )
  );

  //$l_xmls = file_get_contents(dirname(__FILE__) . '/WebService.xml');
  //$l_xmls = file_get_contents($l_wsdl);
  //libxml_disable_entity_loader(false);
  $params = array(
  'trace' => true,
  'location' => $l_location,
  'soap_version' => SOAP_1_2,
  'cache_wsdl' => WSDL_CACHE_NONE,
  'exceptions' => true
  )
  ;
  $client = new SoapClient($l_wsdl, $params);
  $client->__setLocation($l_location);
  //Prueba con otor WebServices - Fin

  $l_datos_envio = array('nuntarjeta' => '496321', 'fecha' => '30/05/2020', 'hora' => '17:30:30');
  echo 'Valores Enviados= ';
  var_dump($l_datos_envio);
  //Llamando al metodo
  $res = $client->ConsultaDescuento($l_datos_envio);
  echo '</br>';
  echo '</br>';

  echo 'esctructura recibida =';
  var_dump($res);
  echo '</br>';
  echo '</br>';
  echo 'codigo: ';
  echo $res->ConsultaDescuentoResult->Descuento->Codigo;
  echo '</br>';
  echo $res->ConsultaDescuentoResult->Descuento->Descripcion;
  echo '</br>';
  echo $res->ConsultaDescuentoResult->Descuento->Porcentaje;

  echo '</br>';
  echo '</br>';
  echo $res->ConsultaDescuentoResult->Errws->TipoErr;
  echo '</br>';
  echo $res->ConsultaDescuentoResult->Errws->CodigoErr;
  echo '</br>';
  echo $res->ConsultaDescuentoResult->Errws->DescripcionErr;

  echo '</br>';

  do_action('logger', $client);
  */
//
 /*

      //Agregar evento php pen checkout procees y Fees
      //add_action('woocommerce_cart_calculate_fees', array($this, 'f_add_discount'), 10); ///
      //add_action('woocommerce_checkout_process', array($this, 'f_validate_realizar_pedido'));
      //Agregar funciones de eventos ajax
      //add_action('wp_ajax_nopriv_event_list2', array($this, 'my_event_list_cb2'));
      //add_action('wp_ajax_event_list2', array($this, 'my_event_list_cb2'));
      //

      function my_event_list_cb2() {
      //l_bin
      // Check for nonce security
      $nonce = sanitize_text_field($_POST['nonce']);
      $l_bin = sanitize_text_field($_POST['l_bin']);

      if (!wp_verify_nonce($nonce, 'my-ajax-nonce')) {
      die('No autorizado!!'); ///Mejorar
      }


      global $woocommerce;
      $abc_prueba = $woocommerce->cart->cart_contents_total;


      //global $woocommerce;

      $l_discount_name = 'Descuento Pago Tarjeta BCP';
      $l_percentage_discount = 0.10;

      $l_discount_name_to_show = $l_discount_name . ' (-' . ($l_percentage_discount * 100) . '%)';

      $surcharge = ( $woocommerce->cart->cart_contents_total + $woocommerce->cart->shipping_total ) * (-1) * $l_percentage_discount;
      $woocommerce->cart->add_fee($l_discount_name_to_show, $surcharge, true, '');



      $l_resultado = $this->f_ws_customer_gets_discount($l_bin);
      if (isset($l_resultado)) {
      $l_codigo = $l_resultado->ConsultaDescuentoResult->Descuento->Codigo;
      if ($l_codigo > 0) {
      $l_descripcion = $l_resultado->ConsultaDescuentoResult->Descuento->Descripcion;
      $l_porcentaje = $l_resultado->ConsultaDescuentoResult->Descuento->Porcentaje;
      echo 'Por el pago con su tarjeta, tiene descuento:' . $l_descripcion . '. de :' . $l_porcentaje . '%.' . $abc_prueba;
      } else {
      echo 'sin descuento:' . $l_descripcion . '. de :' . $l_porcentaje . '%.';
      }
      } else {
      echo 'No deovolvio datos';
      }
      }
     */



    /*
      public function f_add_discount($cart) {
      if (is_admin() && !defined('DOING_AJAX') || is_cart()) {
      return;
      }

      if (isset($_POST['post_data'])) {
      parse_str($_POST['post_data'], $post_data);
      } else {
      $post_data = $_POST; // fallback for final checkout (non-ajax)
      }

      if (isset($post_data['is_wc_h_number'])) {
      $aux = $post_data['is_wc_h_number'];
      //is_wc_h_number

      if ($aux > 0) {

      $l_discount_name = 'Descuento Pago Tarjeta BCP';
      $l_percentage_discount = 10;
      $l_discount_name_to_show = $l_discount_name . ' (-' . ($l_percentage_discount ) . '%)';

      $cart_discount = $this->f_calculate_discount_amount($l_percentage_discount, $cart->cart_contents_total) * - 1;
      $cart->add_fee($l_discount_name_to_show, $cart_discount, true);
      //////$this->f_ws_customer_gets_discount();
      }
      }
      }
     */

    /*
      public function f_add_discount2() {
      global $woocommerce;

      if (is_admin() && !defined('DOING_AJAX')) {
      return;
      }

      $l_discount_name = 'Descuento Pago Tarjeta BCP';
      $l_percentage_discount = 0.10;

      $l_discount_name_to_show = $l_discount_name . ' (-' . ($l_percentage_discount * 100) . '%)';

      $surcharge = ( $woocommerce->cart->cart_contents_total + $woocommerce->cart->shipping_total ) * (-1) * $l_percentage_discount;
      $woocommerce->cart->add_fee($l_discount_name_to_show, $surcharge, true, '');
      }
     */

    /*
     * f_ws_customer_gets_discount: Metodo para consumir el WS descuento 
     */

    /*
      public function f_ws_customer_gets_discount($l_bin) {

      date_default_timezone_set('America/Lima');
      $l_hoy_fecha = date("d/m/Y");
      $l_hoy_hora = date("H:i:s");


      //do_action('logger', '$l_hoy_fecha=' . $l_hoy_fecha);
      //do_action('logger', '$l_hoy_hora=' . $l_hoy_hora);
      //do_action('logger', '$l_bin=' . $l_bin);


      //[01] Definicion de valores de WS
      $l_servidor = 'http://app01.ide-solution.com';
      $l_puerto = '80';
      $l_contexto = 'ws_consulta_descuento';
      $l_servicio = 'WebService';

      //[02] Obteniendo nombres de wsdl y end point
      $l_nombre_wsdl = $l_servicio . '.asmx?wsdl';
      $l_nombre_asmx = $l_servicio . '.asmx';

      //[03] Generando nombre de WSL y location
      $l_wsdl = $l_servidor . ':' . $l_puerto . '/' . $l_contexto . '/' . $l_nombre_wsdl;
      $l_location = $l_servidor . ':' . $l_puerto . '/' . $l_contexto . '/' . $l_nombre_asmx;

      //[04] Parametros de conexion de WS
      $l_params = array(
      'trace' => true,
      'location' => $l_location,
      'soap_version' => SOAP_1_2,
      'cache_wsdl' => WSDL_CACHE_NONE,
      'exceptions' => true
      )
      ;

      //[05] Conectando con WS
      $client = new SoapClient($l_wsdl, $l_params);

      //[06] Preparando valores de envio - OBTENER DEL JAVA SCRIPT
      // $l_datos_envio = array('nuntarjeta' => '496321', 'fecha' => '30/05/2020', 'hora' => '17:30:30');
      //$l_datos_envio = array('nuntarjeta' => $l_bin, 'fecha' => '30/05/2020', 'hora' => '17:30:30');
      $l_datos_envio = array('nuntarjeta' => $l_bin, 'fecha' => $l_hoy_fecha, 'hora' => $l_hoy_hora);
      //[06] consumiendo Metodo ConsultaDescuento del WS
      $result = $client->ConsultaDescuento($l_datos_envio);
      return $result;
      }
     */




    /*
      function f_validate_realizar_pedido() {

      if (!empty($_POST['payment_method']) && $_POST['payment_method'] == 'woo-mercado-pago-custom') {
      $l_codigo_descuento = 0;
      $l_descripcion_descuento = '';
      $l_porcentaje_descuento = 0;

      $l_bin_numero = $_POST['is_wc_h_number'];

      wc_add_notice('$l_bin_numero:' . $l_bin_numero, 'notice');

      $l_resultado_descuento = $this->f_ws_customer_gets_discount('4963211212');
      //var_dump($resultado_descuento);
      if ($resultado_descuento->ConsultaDescuentoResult->Errws->TipoErr == 0) {
      $descripcion_descuento = $resultado_descuento->ConsultaDescuentoResult->Descuento->Descripcion;
      $this->f_add_discount2();
      } else {
      $descripcion_descuento = 'NO HAY';
      }

      $l_mensaje_mostrar = 'Tiene usted el descuento==' . $descripcion_descuento;
      wc_add_notice($l_mensaje_mostrar, 'notice');
      //wc_add_notice($l_mensaje_mostrar, 'success');
      //mp-card-number
      //cardNumber
      }

      //$tarjeta_prueba=WC()->checkout()->getValue('mp-card-number');}
      //$tarjeta_prueba = WC()->checkout()->getValue('mp-card-number');

      wc_add_notice('Identificador de Metodo de Pago: ' . $_POST['payment_method'] . '==', 'error');
      wc_add_notice("No enviar. Aun", 'error');
      }
     */

    /**
     * Add a 1% surcharge to your cart / checkout
     * change the $percentage to set the surcharge to a value to suit
     */
    /* PRUEBA
      public function woocommerce_custom_surcharge01() {
      global $woocommerce;
      do_action('logger', 'Inicio ....');

      if (is_admin() && !defined('DOING_AJAX')) {
      return;
      }

      $l_discount_name = 'Descuento Pago Tarjeta BCP';
      $l_percentage_discount = 0.10;

      $l_discount_name_to_show = $l_discount_name . ' (-' . ($l_percentage_discount * 100) . '%)';

      $surcharge = ( $woocommerce->cart->cart_contents_total + $woocommerce->cart->shipping_total ) * (-1) * $l_percentage_discount;
      $woocommerce->cart->add_fee($l_discount_name_to_show, $surcharge, true, '');
      do_action('logger', 'prueba 02...' . $l_discount_name);
      }
     */


// METODOS DE APOYO -- PARA ELIMINAR - INICIO

    /**
     * Funciones para agregar campos personalizados
     * Select
     */
    /*
      function claserama_add_select_prefered_contact_method($checkout) {
      woocommerce_form_field('contactmethod', array(
      'type' => 'select', //textarea, text,select, radio, checkbox, password
      'required' => true, //este parámetro no valida, solo agrega un "*" al campo
      'class' => array('form-row-wide'), // un array puede ser la clase 'form-row-wide', 'form-row-first', 'form-row-last'
      'label' => 'Método de contacto preferido',
      'options' => array(//opciones para un select o un input radio
      '' => 'Selecciona uno',
      'email' => 'Por email', // valor => nombre
      'telefono' => 'Por teléfono'
      )
      ), $checkout->get_value('contactmethod')
      );
      }
     */

    /**
     * Funciones para agregar campos personalizados
     * Checkbox después de los comentarios de la orden
     */
    /*
      function claserama_add_newsletter_optin($checkout) {
      woocommerce_form_field('subscribe', array(
      'type' => 'checkbox',
      'class' => array('form-row-wide'),
      'label' => ' Suscribirte al Newsletter'
      ), $checkout->get_value('subscribe'));
      }
     */

    /**
     * Validar el campo select que agregamos
     */
    /*
      function claserama_validate_select_field() {
      //Verificar que el campo haya sido seleccionado, si no mostrar un error
      //do_action('logger', print_r($_POST));
      if (empty($_POST['contactmethod'])) {
      wc_add_notice('Por favor selecciona tu método preferido de contacto.', 'error');
      }
      do_action('logger', print_r($_POST));
      //WC()->checkout()->getValue('your-id-card-number');
      }
     */

// METODOS DE APOYO -- PARA ELIMINAR - FIN