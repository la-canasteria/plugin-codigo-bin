<?php

if (!defined('ABSPATH')) {
    exit;
}

/**
 * Class cl-is-wc-pg-discounts
 */
class Cl_Is_Wc_Pg_Discounts {

    /**
     * Instance of this class.     
     * @var object
     */
    protected static $instance = null;

    /**
     * Initialize the plugin.
     */
    public function __construct() {
        try {
            $this->load_dependancy();
        } catch (Exception $e) {
            
        }
    }

    /**
     * Return an instance of this class.
     */
    public static function get_instance() {
        // If the single instance hasn't been set, set it now.
        if (null == self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    protected function load_dependancy() {        
        include_once( 'Cl-Is-Wc-Pg-Add-Discounts.php' );        
    }
}